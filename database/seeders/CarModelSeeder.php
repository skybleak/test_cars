<?php

namespace Database\Seeders;


use App\Models\CarModel;
use Illuminate\Database\Seeder;


final class CarModelSeeder extends Seeder
{
    const RECORDS = [
        [
            'brand_id' => 1,
            'name' => 'G80',
        ],
        [
            'brand_id' => 1,
            'name' => 'GV70',
        ],
        [
            'brand_id' => 1,
            'name' => 'GV80',
        ],
        [
            'brand_id' => 1,
            'name' => 'G70',
        ],
        [
            'brand_id' => 2,
            'name' => 'Getz',
        ],
        [
            'brand_id' => 2,
            'name' => 'i10',
        ],
        [
            'brand_id' => 2,
            'name' => 'Solaris',
        ],
        [
            'brand_id' => 3,
            'name' => 'Picanto',
        ],
        [
            'brand_id' => 3,
            'name' => 'Rio',
        ],
        [
            'brand_id' => 3,
            'name' => 'Soul',
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::RECORDS as $item) {
            $record = new CarModel();
            $record->brand_id = $item['brand_id'];
            $record->name = $item['name'];
            $record->save();
        }
    }
}
