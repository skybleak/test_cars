<?php

namespace Database\Seeders;


use App\Models\Car;
use App\Models\User;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CarBrandSeeder::class);
        $this->call(CarModelSeeder::class);
        Car::factory()->count(50)->create();
        User::factory()->count(20)->create();
    }
}
