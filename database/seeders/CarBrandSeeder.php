<?php

namespace Database\Seeders;


use App\Models\CarBrand;
use Illuminate\Database\Seeder;


final class CarBrandSeeder extends Seeder
{
    const RECORDS = [
        'Genesis',
        'Hyundai',
        'Kia',
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::RECORDS as $item) {
            $record = new CarBrand();
            $record->name = $item;
            $record->save();
        }
    }
}
