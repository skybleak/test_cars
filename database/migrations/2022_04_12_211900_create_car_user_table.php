<?php


use App\Models\Abstracts\MigrationAbstract;
use App\Models\Car;
use App\Models\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;


return new class extends MigrationAbstract
{
    protected string $model = Car::class;


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection())->create(sprintf('%s_%s', Str::singular($this->table()), Str::singular((new User)->getTable())), function (Blueprint $table) {
            $table->bigInteger('car_id')->unsigned()->comment('Идентификатор автомобиля');
            $table->foreign('car_id')->references('id')->on($this->table(Car::class))->restrictOnDelete();

            $table->bigInteger('user_id')->unsigned()->comment('Идентификатор пользователя');
            $table->foreign('user_id')->references('id')->on($this->table(\App\Models\User::class))->restrictOnDelete();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection())->dropIfExists(sprintf('%s_%s', Str::singular($this->table()), Str::singular((new User)->getTable())));
    }
};
