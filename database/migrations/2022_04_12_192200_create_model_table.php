<?php


use App\Models\Abstracts\MigrationAbstract;
use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


return new class extends MigrationAbstract
{
    protected string $model = CarModel::class;


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection())->create($this->table(), function (Blueprint $table) {
            $table->id()->comment('Идентификатор');

            $table->bigInteger('brand_id')->unsigned()->comment('Идентификатор бренда');
            $table->foreign('brand_id')->references('id')->on($this->table(CarBrand::class))->restrictOnDelete();

            $table->string('name', 100)->index()->comment('Название модели');

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection())->dropIfExists($this->table());
    }
};
