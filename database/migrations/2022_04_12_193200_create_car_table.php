<?php


use App\Models\Abstracts\MigrationAbstract;
use App\Models\Car;
use App\Models\CarModel;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


return new class extends MigrationAbstract
{
    protected string $model = Car::class;


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection())->create($this->table(), function (Blueprint $table) {
            $table->id()->comment('Идентификатор');

            $table->bigInteger('model_id')->unsigned()->comment('Идентификатор модели');
            $table->foreign('model_id')->references('id')->on($this->table(CarModel::class))->restrictOnDelete();

            $table->string('number', 15)->index()->comment('Номер автомобиля');

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection())->dropIfExists($this->table());
    }
};
