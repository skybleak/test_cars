<?php


use App\Models\Abstracts\MigrationAbstract;
use App\Models\CarBrand;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


return new class extends MigrationAbstract
{
    protected string $model = CarBrand::class;


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection())->create($this->table(), function (Blueprint $table) {
            $table->id()->comment('Идентификатор');
            $table->string('name', 100)->index()->comment('Название бренда');

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection())->dropIfExists($this->table());
    }
};
