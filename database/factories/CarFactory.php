<?php

namespace Database\Factories;

use App\Models\Car;
use App\Models\CarModel;
use Illuminate\Database\Eloquent\Factories\Factory;


/**
 * @extends Factory<Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'model_id' => CarModel::inRandomOrder()->first()->id,
            'number' => sprintf('%s%03d%s%02d',
                $this->faker->randomElement(['A', 'B', 'E', 'K', 'M', 'H', 'O', 'P', 'T', 'Y', 'X']),
                $this->faker->numberBetween(1, 999),
                implode('', $this->faker->randomElements(['A', 'B', 'E', 'K', 'M', 'H', 'O', 'P', 'T', 'Y', 'X'], 2)),
                $this->faker->numberBetween(0, 99),
            )
        ];
    }
}
