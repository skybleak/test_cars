@extends('layouts.app')

@section('title', 'Аренда автомобиля')

@section('content_header')
    <h1>Отчет</h1>
@stop

@section('content')
    <div class="container">
        <div class="col-12 row">
            <div class="form-group col-12">
                <form>
                    @csrf
                    <div class="col-12 row">
                        <div class="col-12">
                            <label><h3>Аренда автомобиля</h3></label>
                        </div>

                        <div class="col-12">
                            <label for="car">Автомобиль</label>
                            <select id="car">
                                @foreach($cars as $car)
                                    <option value="{{ $car->id }}">{{ sprintf('%s %s (номер %s)', $car->model->name, $car->model->brand->name, $car->number)  }}</option>
                                @endforeach
                            </select>

                            <label for="user">Пользователь</label>
                            <select id="user">
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary" id="btnRent">Арендовать</button>
                        </div>
                    </div>
                </form>
            </div>

            <div id="response"></div>
        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('/js/jquery.js') }}"></script>

    <script>
        $( document ).ready(function() {
            function rent() {
                $.ajax({
                    url: "{{ route('api.rentCar') }}",
                    method: 'GET',
                    data: {
                        car_id: $('#car').val(),
                        user_id: $('#user').val()
                    },
                    success: function(response) {
                        $("#response").html(response.data.message);
                    },
                    error: function(jqxhr) {
                        $("#response").html('');
                        let messages = jQuery.parseJSON(jqxhr.responseText).errors.messages;

                        jQuery.each( messages, function( i, val ) {
                            $("#response").append('Ошибка ' + ++i + ': ' + val + '<br>');
                        });
                    }
                });
            }

            $("#btnRent").click(function(e) {
                e.preventDefault();
                rent();
            });
        });
    </script>
@endsection
