# test_car

Задание:

Даны два списка. Список автомобилей и список пользователей.
C помощью laravel сделать api для управления списком использования автомобилей пользователями.
В один момент времени 1 пользователь может управлять только одним автомобилем. В один момент времени 1 автомобилем может управлять только 1 пользователь.

[Страница с арендой автомобилей](http://127.0.0.1:8000/rent)

Запуск:
```angular2html
composer i
php artisan migrate
php artisan db:seed
```

Тесты:
```angular2html
composer test-sof
```

```angular2html
composer test
```

Coverage тест:
```angular2html
composer test-coverage
```
