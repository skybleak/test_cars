<?php

namespace App\Models;


use App\Models\Abstracts\ModelAbstract;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;


/**
 * Class CarBrand
 *
 * @package App\Models
 * @author skybleak
 * @date 29.08.2022
 *
 * @property int $id Идентификатор
 * @property string $name Название бренда
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 *
 * @method static Builder|CarBrand newModelQuery()
 * @method static Builder|CarBrand newQuery()
 * @method static \Illuminate\Database\Query\Builder|CarBrand onlyTrashed()
 * @method static Builder|CarBrand query()
 * @method static Builder|CarBrand whereCreatedAt($value)
 * @method static Builder|CarBrand whereDeletedAt($value)
 * @method static Builder|CarBrand whereId($value)
 * @method static Builder|CarBrand whereName($value)
 * @method static Builder|CarBrand whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CarBrand withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CarBrand withoutTrashed()
 * @mixin Eloquent
 */
final class CarBrand extends ModelAbstract {
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $table = 'brands';


    /**
     * @return array
     *
     * @author skybleak
     * @date 29.08.2022
     */
    public function tableColumnsMeta(): array {
        return [
            'id' => [
                'label' => 'Идентификатор',
                'casts' => 'integer',
                'fillable' => false,
            ],
            'name' => [
                'label' => 'Бренд автомобиля',
                'casts' => 'string',
                'fillable' => true,
            ],
        ];
    }
}
