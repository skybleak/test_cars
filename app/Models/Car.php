<?php

namespace App\Models;


use App\Models\Abstracts\ModelAbstract;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Database\Factories\CarFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;


/**
 * Class Car
 *
 * @package App\Models
 * @author skybleak
 * @date 29.08.2022
 *
 * @property int $id Идентификатор
 * @property int $model_id Идентификатор модели
 * @property string $number Номер автомобиля
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 *
 * @property-read User|null $users
 *
 * @method static CarFactory factory(...$parameters)
 * @method static Builder|Car newModelQuery()
 * @method static Builder|Car newQuery()
 * @method static \Illuminate\Database\Query\Builder|Car onlyTrashed()
 * @method static Builder|Car query()
 * @method static Builder|Car whereCreatedAt($value)
 * @method static Builder|Car whereDeletedAt($value)
 * @method static Builder|Car whereId($value)
 * @method static Builder|Car whereModelId($value)
 * @method static Builder|Car whereNumber($value)
 * @method static Builder|Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Car withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Car withoutTrashed()
 * @mixin Eloquent
 */
final class Car extends ModelAbstract {
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $table = 'cars';


    /**
     * @return array
     *
     * @author skybleak
     * @date 29.08.2022
     */
    public function tableColumnsMeta(): array {
        return [
            'id' => [
                'label' => 'Идентификатор',
                'casts' => 'integer',
                'fillable' => false,
            ],
            'number' => [
                'label' => 'Номер автомобиля',
                'casts' => 'string',
                'fillable' => true,
            ],
            'model_id' => [
                'label' => 'Модель автомобиля',
                'casts' => 'integer',
                'fillable' => true,
            ],
        ];
    }


    public function users(): belongsToMany {
        return $this->belongsToMany(User::class);
    }


    public function model(): BelongsTo {
        return $this->belongsTo(CarModel::class);
    }
}
