<?php

namespace App\Models;


use App\Models\Abstracts\ModelAbstract;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;


/**
 * Class CarModel
 *
 * @package App\Models
 * @author skybleak
 * @date 29.08.2022
 *
 * @property int $id Идентификатор
 * @property int $brand_id Идентификатор бренда
 * @property string $name Название модели
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 *
 * @method static Builder|CarModel newModelQuery()
 * @method static Builder|CarModel newQuery()
 * @method static \Illuminate\Database\Query\Builder|CarModel onlyTrashed()
 * @method static Builder|CarModel query()
 * @method static Builder|CarModel whereBrandId($value)
 * @method static Builder|CarModel whereCreatedAt($value)
 * @method static Builder|CarModel whereDeletedAt($value)
 * @method static Builder|CarModel whereId($value)
 * @method static Builder|CarModel whereName($value)
 * @method static Builder|CarModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CarModel withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CarModel withoutTrashed()
 * @mixin Eloquent
 */
final class CarModel extends ModelAbstract {
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $table = 'models';


    /**
     * @return array
     *
     * @author skybleak
     * @date 29.08.2022
     */
    public function tableColumnsMeta(): array {
        return [
            'id' => [
                'label' => 'Идентификатор',
                'casts' => 'integer',
                'fillable' => false,
            ],
            'brand_id' => [
                'label' => 'Бренд автомобиля',
                'casts' => 'integer',
                'fillable' => true,
            ],
            'model' => [
                'label' => 'Модель автомобиля',
                'casts' => 'string',
                'fillable' => true,
            ],
        ];
    }


    public function brand(): BelongsTo {
        return $this->belongsTo(CarBrand::class);
    }
}
