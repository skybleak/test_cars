<?php

namespace App\Models\Abstracts;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


/**
 * Class ModelAbstract
 * @package App\Models\Abstracts
 *
 * @author skybleak
 * @date 29.08.2022
 */
abstract class ModelAbstract extends Model {
    /**
     * Список колонок в таблице модели
     * @var array|string[]
     */
    public static array $tableColumns = [];


    /**
     * @throws Exception
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);

        // @codeCoverageIgnoreStart
        if (App::environment('testing'))
            $this->connection = 'sqlite';
        // @codeCoverageIgnoreEnd

        $tableColumnsMeta = $this->getTableColumnsMeta();

        $tableColumnsFilled = (bool)count(self::$tableColumns);
        foreach ($tableColumnsMeta as $key => $value) {
            if ($value['fillable'])
                $this->fillable[] = $key;

            if (!$tableColumnsFilled)
                self::$tableColumns[$key] = strlen($value['label']) ? $value['label'] : $key;

            if (strlen($value['casts']))
                $this->casts[$key] = $value['casts'];
        }
    }


    /**
     * Получить заголовок поля для UI
     *
     * @param string $columnName
     * @return string
     */
    public static function getTableColumnLabel(string $columnName): string {
        /** @var array $tableColumns */
        $called_class = get_called_class();

        if (isset($called_class::$tableColumns))
            if (!count($called_class::$tableColumns))
                $t = new $called_class;

        if (isset($called_class::$tableColumns[$columnName]) && strlen($called_class::$tableColumns[$columnName]))
            return $called_class::$tableColumns[$columnName] ?? $columnName;
        else
            return $columnName;
    }


    /**
     * @throws Exception
     */
    public function getTableColumnsMeta(): array {
        if (method_exists($this, 'tableColumnsMeta'))
            return $this->tableColumnsMeta();

        throw new Exception(sprintf('В %s модели отсутствует метод tableColumnsMeta()', get_class($this)));
    }
}
