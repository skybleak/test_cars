<?php

namespace App\Models\Abstracts;


use App\Models\Interfaces\MigrationInterface;
use Illuminate\Database\Migrations\Migration;


/**
 * Class MigrationAbstract
 * @package App\Models\Abstracts
 *
 * @author skybleak
 * @date 29.08.2022
 */
abstract class MigrationAbstract extends Migration implements MigrationInterface {
    protected string $model = '';


    public function table(?string $tableName = null): string {
        if (is_null($tableName))
            return (new $this->model())->getTable();
        else
            return (new $tableName)->getTable();
    }


    public function connection(?string $connectionName = null): string {
        if (is_null($connectionName)) {
            if (is_null((new $this->model())->getConnectionName()))
                return config('database.connections.mysql.database');
            else
                return (new $this->model())->getConnectionName();
        } else
            return $connectionName;
    }
}
