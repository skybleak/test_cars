<?php

namespace App\Models\Interfaces;


interface MigrationInterface {
    public function table(?string $tableName = null): string;

    public function connection(?string $connectionName = null): string;
}
