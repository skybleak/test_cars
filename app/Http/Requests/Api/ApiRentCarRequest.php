<?php

namespace App\Http\Requests\Api;


use JetBrains\PhpStorm\ArrayShape;


final class ApiRentCarRequest extends APIFormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'user_id' => 'integer',
        'car_id' => 'integer',
    ])]
    public function rules(): array {
        return [
            'user_id' => 'required|exists:App\Models\User,id',
            'car_id' => 'required|exists:App\Models\Car,id',
        ];
    }
}
