<?php

namespace App\Http\Controllers;


use App\Models\Car;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;


class RentController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return View|Factory
     */
    public function rent(): Factory|View {
        $users = User::all();
        $cars = Car::all();

        return view('rent', compact('users', 'cars'));
    }
}
