<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


/**
 * Class Controller
 * @package App\Http\Controllers
 *
 * @OA\Info(
 *     title="API документация",
 *     version="0.0.1",
 * )
 *
 * @author skybleak
 * @date 29.08.2022
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
