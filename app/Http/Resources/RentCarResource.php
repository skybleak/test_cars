<?php

namespace App\Http\Resources;


use App\Models\Car;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


/**
 * Class RentCarResource
 * @package App\Modules\Pands\Http\Resources
 *
 * @author skybleak
 * @date 29.08.2022
 *
 * @mixin Car
 */
class RentCarResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'message' => sprintf('Пользователь %s арендовал автомобиль %s %s с номером %s',
                $this->users->first()->name,
                $this->model->brand->name,
                $this->model->name,
                $this->number,
            ),
        ];
    }
}
